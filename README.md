# GTAVSnapConvert

### Overview:

GTAVSnapConvert is a small tool to convert Grand Theft Auto V Snapmatic files to a standard .jpg files.  GTAVSnapConvert is compatible with Python 3.0 or above.

### Usage:

For a simple GUI run "python.exe GTAVSnapConvert.py".  Choose your input files and output path, then click the "Convert!" button.


### Notes:

Images taken with the in-game Snapmatic feature are limited to a maximum resolution of 960x536.


### Legal:
Copyright (C) 2024 Dann Moore.  All rights reserved.




