#
# File: GTAVSnapConvert.py
# Description: Converts Grand Theft Auto V Snapmatic files to a standard .jpg files
#
# Copyright (C) 2020 Dann Moore.  All rights reserved.
#

import tkinter as tk
from tkinter import messagebox
from tkinter import filedialog
import os
import sys


# define callback for our select input file button
def input_buttonCallBack():
   global filenames
   
   infilenames = filedialog.askopenfilenames(initialdir = "./",title = "Select input file",filetypes = (("Snapmatic files","PGTA*.*"),("All files","*.*")))

   if infilenames != "":
      infilename_label["text"] = "Input File(s) Selected"
      filenames = list(infilenames)
#



# define callback for our select output file button
def output_buttonCallBack():
   global outpath
   outpath = filedialog.askdirectory(initialdir = "./", title = "Select output path")
   outpath_label["text"] = "Output Path: " + outpath
#




# define callback for our convert button
def convert_buttonCallBack():
   global filenames
   global outpath

   if not filenames:
      messagebox.showinfo("Error", "Please select input file(s)...")
   else:
      for infilename in filenames:
         outfilename = outpath + "/" + os.path.basename(infilename) + ".jpg"
         doconversion(infilename, outfilename)

      messagebox.showinfo("Completed", "Conversion complete!")
#




# define the conversion function
def doconversion(filename_in, filename_out):
   print("Processing " + filename_in)
   fout = open(filename_out, "wb")
   with open(filename_in, "rb") as fin:
      # First 292 bytes are metadata and can be ignored
      fin.read(292)

      # Read each byte from the file
      byte1 = fin.read(1)
      while byte1:
        fout.write(byte1)
        lastbyte = byte1   # Save the previous byte
        byte1 = fin.read(1)

        # The JPEG format always ends with "FF D9", break out if we find this sequence
        if (lastbyte == b'\xFF') and (byte1 == b'\xD9'):
           fout.write(byte1)
           break

      
   fin.close()
   fout.close()
   print("Conversion complete!")
#






# Initialize variables
filenames = []
outpath = "."


# create tkinter controls
root = tk.Tk()
root.title("GTAVSnapConvert")


infilename_label = tk.Label(root, text="Select Input File(s)")
infilename_label.pack()


input_button = tk.Button(root, text ="Choose Input File(s)", command = input_buttonCallBack)
input_button.pack()

outpath_label = tk.Label(root, text="Output Path: " + outpath)
outpath_label.pack()

output_button = tk.Button(root, text ="Choose Output Path", command = output_buttonCallBack)
output_button.pack()

convert_button = tk.Button(root, text ="Convert!", command = convert_buttonCallBack)
convert_button.pack()


# begin tkinter gui loop
root.mainloop()

